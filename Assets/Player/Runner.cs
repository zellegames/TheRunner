using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class Runner : MonoBehaviour
    {
        public enum AimDirection { NONE, FORWARD, UP, DOWN, UP_FORWARD, DOWN_FORWARD }
        public Rigidbody2D RB { get { return rb; } }
        public bool Sliding { get { return sliding; } }
        public bool WallKick;

        [SerializeField]
        StateMachine statemachine;

        [SerializeField]
        GameObject debugPanel;

        [Header("Audio")]
        [SerializeField]
        AudioSource slideSource;

        [SerializeField]
        AudioClip thump;

        [SerializeField]
        AudioClip slap;

        [SerializeField]
        AudioClip jump;

        [SerializeField]
        AudioClip step1;

        [SerializeField]
        AudioClip step2;

        [SerializeField]
        AudioClip damage;

        [SerializeField]
        AudioClip slide;

        [SerializeField]
        AudioClip explode;

        [SerializeField]
        AudioClip preExplode;

        AimDirection aimDirection;
        AimDirection lastAimDirection;
        Animator animator;
        AudioSource aSource;
        Rigidbody2D rb;
        SpriteRenderer spriteRenderer;
        StateMachine.State lastState;
        bool airborneLastFrame;
        bool jumpingLastFrame;
        bool walledLastFrame;
        bool step1Played;
        bool sliding;
        bool slideBoost;
        float slideSpeed;
        Vector2 moveDirection;
        GameObject dyingParticles;
        GameObject deathParticles;
        GameObject wallkickParticles;
        ParticleSystem sparkParticles;

        // Launch state
        bool launched;
        bool readyToLaunch;
        bool launchReverse;
        float launchPower;
        Collider2D launcherCollider;
        Vector2 launchDirection;

        public void Move()
        {
            if (statemachine.CurrentState == StateMachine.State.DED || statemachine.CurrentState == StateMachine.State.GOAL)
            {

                if (statemachine.CurrentState == StateMachine.State.GOAL)
                {
                    spriteRenderer.enabled = false;
                }

                rb.velocity = Vector2.zero;
                sparkParticles.Stop();
                return;
            }

            if (launched)
            {
                statemachine.SetReverse(launchReverse);
                launched = false;
                readyToLaunch = true;
                return;
            }

            if (readyToLaunch)
            {
                rb.velocity += launchDirection * launchPower;
                readyToLaunch = false;
                return;
            }

            var velo = rb.velocity;
            var dir = (statemachine.Reverse) ? -moveDirection : moveDirection;
            var accel = (dir.y > 0f) ? statemachine.PlayerData.Acceleration * 2f : statemachine.PlayerData.Acceleration;
            
            velo +=  dir * accel * Time.deltaTime;
            Debug.DrawRay(transform.position, dir, Color.red);

            if (statemachine.CurrentState == StateMachine.State.KNOCKBACK)
            {
                velo.x = statemachine.PlayerData.KnockbackSpeed;
                velo.y = 0f;
                if (!statemachine.KnockbackRight)
                {
                    velo.x = -velo.x;
                }
                rb.velocity = velo;
                return;
            }

            if (statemachine.CurrentState == StateMachine.State.IDLE)
            {
                rb.velocity = Vector2.zero;
                return;
            }

            if (velo.x > statemachine.PlayerData.MaxSpeed)
            {
                velo.x -= statemachine.PlayerData.Acceleration * 2f * Time.deltaTime;
            }

            if (velo.x < -statemachine.PlayerData.MaxSpeed)
            {
                velo.x += statemachine.PlayerData.Acceleration * 2f * Time.deltaTime;
            }

            if (velo.y < -10f)
            {
                velo.y = -10f;
            }

            if (WallKick)
            {
                velo.x = statemachine.PlayerData.MaxSpeed;
                if (statemachine.Reverse) velo.x = -velo.x;
                aSource.PlayOneShot(slap, 0.2f);
                Instantiate(wallkickParticles, (Vector2)transform.position + new Vector2(0f, -0.5f), Quaternion.identity);
                WallKick = false;
            }

            if (statemachine.IsJumping)
            {
                if (!jumpingLastFrame)
                {
                    aSource.PlayOneShot(jump);
                    Instantiate(wallkickParticles, (Vector2)transform.position + new Vector2(0f, -0.5f), Quaternion.Euler(0f, 0f, -110f));
                    jumpingLastFrame = true;
                }
                velo.y = statemachine.PlayerData.JumpForce;
            } 
            else if (sliding)
            {
                var boost = (slideBoost) ? 10f : 3f;

                if (velo.magnitude < slideSpeed)
                {
                    velo += statemachine.PlayerData.Acceleration * boost * Time.deltaTime * dir;
                }

                if (velo.magnitude > slideSpeed)
                {
                    velo -= statemachine.PlayerData.Acceleration * 3f * Time.deltaTime * dir;
                }

                jumpingLastFrame = false;
            }
            else
            {
                jumpingLastFrame = false;
            }

            if (sliding)
            {
                var e = sparkParticles.emission;
                e.enabled = true;
                if (!slideSource.isPlaying)
                {
                    slideSource.Play();
                }
            } else
            {
                var e = sparkParticles.emission;
                e.enabled = false;
                if (slideSource.isPlaying)
                {
                    slideSource.Stop();
                }
            }

            spriteRenderer.flipX = statemachine.Reverse;
            animator.speed = Mathf.Abs(velo.x) / statemachine.PlayerData.MaxSpeed * 2f;
            rb.velocity = velo;
        }

        public void Launch(Vector2 direction, float power, bool reverse, Collider2D launcher)
        {

            launched = true;
            launchDirection = direction;
            launchPower = power;
            launchReverse = reverse;
            launcherCollider = launcher;
        }

        public void Goal()
        {
            statemachine.SetGoal(true);
        }

        public void TakeDamage(HitData hitData)
        {
            statemachine.PlayerData.SetPendingHit(hitData);
        }

        public IEnumerator CycleFlash()
        {
            while (true)
            {
                var color = spriteRenderer.color;
                color.a = (color.a == 0.25f) ? 1f : 0.25f;
                spriteRenderer.color = color;
                yield return new WaitForSeconds(0.04f);
            }
        }

        public void ResetFlash()
        {
            var color = spriteRenderer.color;
            color.a = 1f;
            spriteRenderer.color = color;
        }

        public void PlayDamageSound()
        {
            aSource.PlayOneShot(damage, 0.2f);
        }

        public void CheckGrounding()
        {
            var hit = Physics2D.BoxCast(transform.position + new Vector3(0f, -0.2f, 0f), GetComponent<Collider2D>().bounds.size * 0.7f, 0f, Vector2.down, 0.35f, LayerMask.GetMask("ground"));
            if (hit)
            {
                if (airborneLastFrame)
                {
                    aSource.PlayOneShot(thump, 0.4f);
                }
                moveDirection = -Vector2.Perpendicular(hit.normal).normalized;
                statemachine.SetGrounded(true);
                statemachine.SetMoveDirection(moveDirection);
                airborneLastFrame = false;
                return;
            }
            else
            {
                airborneLastFrame = true;
                moveDirection = Vector2.right;
                statemachine.SetGrounded(false);
            }

            hit = Physics2D.BoxCast(transform.position + new Vector3(0f, -0.2f, 0f), GetComponent<Collider2D>().bounds.size * 0.8f, 0f, Vector2.right, 0.4f, LayerMask.GetMask("ground"));
            if (hit)
            {
                if (!walledLastFrame)
                {
                    aSource.PlayOneShot(thump);
                    aSource.PlayOneShot(slap, 0.4f);
                    walledLastFrame = true;
                }
                statemachine.SetWalled(true);
                return;
            }
            else
            {
                statemachine.SetWalled(false);
            }

            hit = Physics2D.BoxCast(transform.position + new Vector3(0f, -0.2f, 0f), GetComponent<Collider2D>().bounds.size * 0.8f, 0f, Vector2.left, 0.4f, LayerMask.GetMask("ground"));
            if (hit)
            {
                if (!walledLastFrame)
                {
                    aSource.PlayOneShot(thump, 0.2f);
                    aSource.PlayOneShot(slap, 0.1f);
                    walledLastFrame = true;
                }
                statemachine.SetWalled(true);
                return;
            }
            else
            {
                statemachine.SetWalled(false);
            }
            walledLastFrame = false;
        }

        public void SetSlide(bool sliding, bool reverse, float speed)
        {
            this.sliding = sliding;
            slideSpeed = speed;
            statemachine.SetReverse(reverse);
        }
        public IEnumerator Kill()
        {
            while (true)
            {
                aSource.PlayOneShot(preExplode, 0.6f);
                var p = Instantiate(dyingParticles, transform.position, Quaternion.identity);
                yield return new WaitForSeconds(1f);
                Destroy(p);
                var p2 = Instantiate(deathParticles, transform.position, Quaternion.identity);
                aSource.PlayOneShot(explode, 0.8f);
                spriteRenderer.enabled = false;
                yield return new WaitForSeconds(2f);
                statemachine.KillFinished();
                Destroy(gameObject);
                break;
            }
        }

        public void PickupCoin()
        {
            statemachine.PlayerData.Coins += 1;
        }

        void Awake()
        {
            Application.targetFrameRate = 60;
            SetupControls();
            animator = GetComponent<Animator>();
            rb = GetComponent<Rigidbody2D>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            dyingParticles = Resources.Load<GameObject>("Player/DyingParticles");
            deathParticles = Resources.Load<GameObject>("Player/DeathParticles");
            wallkickParticles = Resources.Load<GameObject>("Player/WallKickParticles");
            aSource = GetComponent<AudioSource>();
            sparkParticles = transform.Find("Sparks").gameObject.GetComponent<ParticleSystem>();
        }

        void Start()
        {
            statemachine.Init();
            statemachine.SetRunner(this);
            debugPanel = GameObject.Find("DebugPanel");
            if (debugPanel)
            {
                debugPanel.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            statemachine.Update();
            HandleAnimations();
            CheckGrounding();
            Move();
            lastState = statemachine.CurrentState;
            lastAimDirection = aimDirection;
        }

        void SetupControls()
        {
            Controls controls = new Controls();
            controls.InGame.Jump.performed += HandleJump;
            controls.InGame.Jump.canceled += HandleJump;

            controls.InGame.Debug.performed += HandleDebug;

            controls.InGame.Enable();

            //controls.InGame.Aim.performed += HandleAim;
            //controls.InGame.Aim.canceled += HandleAim;
        }

        void HandleDebug(InputAction.CallbackContext ctx)
        {
            debugPanel.SetActive(!debugPanel.activeSelf);
        }

        private void HandleAim(InputAction.CallbackContext ctx)
        {
            var v = ctx.ReadValue<Vector2>();
            if (v.x != 0 && v.y == 0)
            {
                aimDirection = AimDirection.FORWARD;
            }

            if (v.x != 0 && v.y > 0)
            {
                aimDirection = AimDirection.UP_FORWARD;
            }

            if (v.x == 0 && v.y == 1)
            {
                aimDirection = AimDirection.UP;
            }

            if (v.x != 0 && v.y < 0)
            {
                aimDirection = AimDirection.DOWN_FORWARD;
            }

            if (v.x == 0 && v.y == -1)
            {
                aimDirection = AimDirection.DOWN;
            }

            if (v.x == 0 && v.y == 0)
            {
                aimDirection = AimDirection.NONE;
            }

        }

        void HandleJump(InputAction.CallbackContext ctx)
        {
            statemachine.SetJumpRequested(ctx.ReadValueAsButton());
        }

        void HandleAnimations()
        {
            if (statemachine.CurrentState == StateMachine.State.KNOCKBACK || statemachine.CurrentState == StateMachine.State.DED)
            {
                animator.SetBool("sliding", false);
                animator.SetBool("jumping", false);
                animator.SetBool("falling", false);
                animator.SetBool("running", false);
                animator.SetBool("airborne", false);
                animator.SetBool("knockback", true);
                return;
            }
            else
            {
                animator.SetBool("knockback", false);
            }

            if (sliding)
            {
                animator.SetBool("sliding", true);
                animator.SetBool("jumping", false);
                animator.SetBool("falling", false);
                animator.SetBool("running", false);
                animator.SetBool("airborne", false);
                return;
            } else
            {
                animator.SetBool("sliding", false);
            }

            animator.SetBool("aiming", aimDirection != AimDirection.NONE);
            animator.SetBool("airborne", (statemachine.CurrentState == StateMachine.State.JUMPING || statemachine.CurrentState == StateMachine.State.FALLING));

            if (lastState != statemachine.CurrentState)
            {
                switch (statemachine.CurrentState)
                {
                    case StateMachine.State.GROUNDED:
                        animator.SetBool("jumping", false);
                        animator.SetBool("falling", false);
                        animator.SetBool("running", true);
                        break;

                    case StateMachine.State.JUMPING:
                        animator.SetBool("jumping", true);
                        animator.SetBool("falling", false);
                        animator.SetBool("running", false);
                        break;

                    case StateMachine.State.FALLING:
                        animator.SetBool("jumping", false);
                        animator.SetBool("falling", true);
                        animator.SetBool("running", false);
                        break;
                }
            }

            if (lastAimDirection != aimDirection || aimDirection == AimDirection.NONE) {
                animator.SetBool("aim_f", false);
                animator.SetBool("aim_dd", false);
                animator.SetBool("aim_d", false);
                animator.SetBool("aim_u", false);
                animator.SetBool("aim_du", false);

                switch (aimDirection)
                {
                    case AimDirection.FORWARD:
                        animator.SetBool("aim_f", true);
                        break;
                    case AimDirection.UP_FORWARD:
                        animator.SetBool("aim_du", true);
                        break;
                    case AimDirection.UP:
                        animator.SetBool("aim_u", true);
                        break;
                    case AimDirection.DOWN_FORWARD:
                        animator.SetBool("aim_dd", true);
                        break;
                    case AimDirection.DOWN:
                        animator.SetBool("aim_d", true);
                        break;
                }
            }
        }

        void PlayStep()
        {
            if (!step1Played)
            {
                aSource.PlayOneShot(this.step1, 0.1f);
                step1Played = true;
            } else
            {
                aSource.PlayOneShot(this.step2, 0.1f);
                step1Played = false;
            }
        }
    }
}