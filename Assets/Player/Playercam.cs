using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playercam : MonoBehaviour
{
    public GameObject Target;
    public float CamSpeed;
    public float CamZoomSpeed;

    [SerializeField]
    Player.StateMachine stateMachine;

    [SerializeField]
    GameObject parallax;

    Rigidbody2D targetRBody;
    Camera cam;

    private void Awake()
    {
        cam = GetComponent<Camera>();
        var start = transform.position;
        start.z = 0;
        parallax.transform.position = start;
    }

    // Update is called once per frame
    void Update()
    {
        var lastPos = transform.position;
        if (Target == null)
        {
            Target = GameObject.FindGameObjectWithTag("Player");
            if (Target != null)
            {
                targetRBody = Target.GetComponent<Rigidbody2D>();
            }
        } 
        else
        {
            var pos = Target.transform.position;
            pos.z = -10f;
            transform.position = pos;
            var d = transform.position - lastPos;
            parallax.transform.Translate(d / 32f);
        }

    }
}
