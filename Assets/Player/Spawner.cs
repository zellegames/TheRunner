using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        GameObject player;

        private void Start()
        {
            Spawn();
        }

        public GameObject Spawn()
        {
            return Instantiate(player, transform.position, Quaternion.identity);
        }
    }
}
