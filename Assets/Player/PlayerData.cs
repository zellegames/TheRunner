using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player {
    [CreateAssetMenu(menuName = "Player Data")]
    public class PlayerData : ScriptableObject
    {
        [SerializeField]
        Player.Serializer serializer;

        [SerializeField]
        float maxSpeed;

        [SerializeField]
        float jumpForce;

        [SerializeField]
        float jumpTime;

        [SerializeField]
        float jumpForgivenessTime;

        [SerializeField]
        float wallkickCooldown;

        [SerializeField]
        float hp;

        [SerializeField]
        float acceleration;

        [SerializeField]
        int coins;

        [SerializeField]
        int coinCount;

        [SerializeField]
        HitData pendingHit;

        [SerializeField]
        float knockbackTime;

        [SerializeField]
        float knockbackSpeed;

        [SerializeField]
        float iTime;

        float missionTimer;

        public float MaxSpeed { get { return maxSpeed; } }
        public float JumpForce { get { return jumpForce; } }
        public float JumpTime { get { return jumpTime; } }
        public float JumpForgivenessTime { get { return jumpForgivenessTime; } }
        public float WallkickCooldown { get { return wallkickCooldown; } }
        public float Acceleration { get { return acceleration; } }
        public int CoinCount {  get { return coinCount; } }
        public float MissionTimer { get { return missionTimer; } }
        public int Coins 
        {
            get
            {
                return coins;
            }

            set
            {
                coins = value;
            }
        }

        public float HP 
        {  
            get { return hp; } 
            set 
            {
                hp = Mathf.Max(0, value); 
            } 
        }

        public HitData PendingHit { get { return pendingHit; } }
        public float KnockbackTime { get { return knockbackTime; } }
        public float KnockbackSpeed { get { return knockbackSpeed; } }
        public float ITime {  get { return iTime; } }

        public void Save()
        {
            serializer.Save(this);
        }

        public void ResetPlayer()
        {
            hp = 100f;
            coins = 0;
            pendingHit = null;
        }

        public void Load()
        {
            serializer.Load(this);
        }

        public void SetPendingHit(HitData hitData)
        {
            pendingHit = hitData;
        }

        public void SetCoinCount(int count)
        {
            coinCount = count;
        }

        public void SetTime(float newTime)
        {
            missionTimer = newTime;
        }

    }

    public abstract class Serializer : MonoBehaviour
    {
        public abstract void Save(PlayerData playerData);
        public abstract PlayerData Load(PlayerData playerData);
    }

    public class HitData
    {
        public float Amount { get; }
        public bool KnockbackRight { get; }

        public HitData(float Amount, bool KnockbackRight)
        {
            this.Amount = Amount;
            this.KnockbackRight = KnockbackRight;
        }
    }
}
