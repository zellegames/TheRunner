using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Player
{
    public class MemorySerializer : Serializer
    {
        string serializedData;

        public override void Save(PlayerData playerData)
        {
            serializedData = JsonUtility.ToJson(playerData);
            Debug.Log("Saved data as " + serializedData);
        }

        public override PlayerData Load(PlayerData playerData)
        {
            var pd = ScriptableObject.CreateInstance<PlayerData>();
            JsonUtility.FromJsonOverwrite(serializedData, pd);
            Debug.Log("Loaded playerdata from serialized string " + pd.ToString());
            return pd;
        }
    }
}
