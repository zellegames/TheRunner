using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Player
{
    [CreateAssetMenu(menuName = "Player State Machine")]
    public class StateMachine : ScriptableObject
    {
        public enum State { IDLE, GROUNDED, JUMPING, FALLING, WALLED, WALLKICK, DED, KNOCKBACK, GOAL }
        public State CurrentState { get { return currentState; } }
        public PlayerData PlayerDataTemplate { get { return playerDataTemplate; } }

        public PlayerData PlayerData { get { return playerData; } }
        public bool IsJumping { get { return isJumping; } }
        public bool JumpReleased { get { return jumpReleased; } }
        public bool Walled { get { return walled; } }
        public bool Reverse { get { return reverse; } }
        public Vector2 MoveDirection { get { return moveDirection; } }
        public Runner Runner { get { return runner; } }

        public bool KnockbackRight { get { return knockbackRight; } }

        [SerializeField]
        PlayerData playerDataTemplate;

        [SerializeField]
        GameObject goalPrefab;

        [SerializeField]
        PlayerData playerData;

        [SerializeField]
        bool isJumping;

        [SerializeField]
        State currentState = State.GROUNDED;

        [SerializeField]
        bool jumpRequested;

        [SerializeField]
        bool jumpReleased = true;

        [SerializeField]
        float jumpTime;

        [SerializeField]
        float jumpForgivenessTimer;

        [SerializeField]
        float wallKickTime;

        [SerializeField]
        bool walled;

        [SerializeField]
        bool grounded;

        [SerializeField]
        float groundedCooldown;

        [SerializeField]
        bool reverse;

        [SerializeField]
        Runner runner;


        Vector2 moveDirection = Vector2.right;
        Coroutine flash;
        bool knockbackRight;
        bool goal;
        float knockbackTimer;
        float iTimer = 5f;
        float idleFrames;

        public void SetGoal(bool goal)
        {
            this.goal = goal;
            if (goal)
            {
                Instantiate(goalPrefab);
            }
        }

        public void SetReverse(bool reverse)
        {
            this.reverse = reverse;
        }

        public void SetGrounded(bool grounded)
        {
            this.grounded = grounded;
        }

        public void SetWalled(bool walled)
        {
            this.walled = walled;
        }

        public void SetJumpRequested(bool requested)
        {
            this.jumpRequested = requested;
        }

        public void SetMoveDirection(Vector2 moveDirection)
        {
            this.moveDirection = moveDirection;
        }

        public void Init()
        {
            goal = false;
            playerData.ResetPlayer();
            playerData.SetCoinCount(GameObject.FindGameObjectsWithTag("Coin").Length);
            isJumping = false;
            jumpRequested = false;
            jumpReleased = true;
            jumpTime = 0f;
            jumpForgivenessTimer = 0f;
            wallKickTime = 0f;
            walled = false;
            grounded = false;
            reverse = false;
        }

        public void SetRunner(Runner runner)
        {
            this.runner = runner;
            currentState = State.FALLING;
        }

        void CheckCanJump()
        {
            if (!jumpRequested)
            {
                jumpReleased = true;
            }
        }

        bool CheckWallJump()
        {
            if (wallKickTime > playerData.WallkickCooldown && walled)
            {
                CheckCanJump();
                if (jumpReleased && jumpRequested)
                {
                    jumpTime = 0f;
                    ToggleReverse();
                    CheckJump();
                    return true;
                }
            }

            return false;
        }

        bool CheckJump()
        {
            if (jumpReleased && jumpRequested)
            {
                jumpReleased = false;
                currentState = State.JUMPING;
                jumpTime = 0f;
                return true;
            }

            return false;
        }

        void ToggleReverse()
        {
            runner.WallKick = true;
            reverse = !reverse;
        }

        public void Update()
        {
            if (!runner || !playerData) { return; }
            wallKickTime += Time.deltaTime;
            iTimer += Time.deltaTime;
            if (currentState != State.DED && currentState != State.GOAL)
            {
                PlayerData.SetTime(PlayerData.MissionTimer + Time.deltaTime);
            }

            if (goal)
            {
                currentState = State.GOAL;
                return;
            }

            if (playerData.HP == 0 && currentState != State.DED)
            {
                currentState = State.DED;
                runner.StartCoroutine(runner.Kill());
            }

            if (iTimer > PlayerData.ITime && PlayerData.PendingHit != null)
            {
                isJumping = false;
                currentState = State.KNOCKBACK;
                playerData.HP -= PlayerData.PendingHit.Amount;
                knockbackRight = PlayerData.PendingHit.KnockbackRight;
                runner.PlayDamageSound();
                iTimer = 0f;
                if (flash == null)
                {
                    flash = runner.StartCoroutine(runner.CycleFlash());
                }
            }
            else
            {
                if (iTimer > PlayerData.ITime && flash != null)
                {
                    runner.StopCoroutine(flash);
                    runner.ResetFlash();
                    flash = null;
                }
            }
            PlayerData.SetPendingHit(null);

            // Previous state dependent states
            switch (CurrentState)
            {
                case State.GOAL:
                    return;
                case State.GROUNDED:
                    CheckCanJump();
                    if (CheckJump()) 
                    { 
                        return;
                    }

                    if (!grounded)
                    {
                        jumpForgivenessTimer += Time.deltaTime;
                        if (jumpForgivenessTimer >= PlayerData.JumpForgivenessTime)
                        {
                            currentState = State.FALLING;
                            return;
                        }
                    } else
                    {
                        jumpForgivenessTimer = 0f;
                    }

                    break;

                case State.JUMPING:
                    jumpTime += Time.deltaTime;
                    if (CheckWallJump()) { return; }

                    if (jumpTime > playerData.JumpTime)
                    {
                        currentState = State.FALLING;
                        isJumping = false;
                        jumpTime = 0f;
                        return;
                    }

                    isJumping = true;
                    if (!jumpRequested)
                    {
                        isJumping = false;
                        currentState = State.FALLING;
                        jumpTime = 0f;
                    }
                    break;

                case State.FALLING:
                    groundedCooldown += Time.deltaTime;
                    if (CheckWallJump()) { return; }
                    if (groundedCooldown > 0.08f && grounded)
                    {
                        groundedCooldown = 0f;
                        currentState = State.GROUNDED;
                    }
                    break;
                case State.KNOCKBACK:
                    knockbackTimer += Time.deltaTime;
                    if (knockbackTimer > PlayerData.KnockbackTime)
                    {
                        currentState = State.IDLE;
                        knockbackTimer = 0f;
                    }
                    break;
                case State.IDLE:
                    if (idleFrames > 0)
                    {
                        currentState = State.FALLING;
                        idleFrames = 0;
                    }

                    idleFrames++;
                    break;
            }
        }

        public void KillFinished()
        {
            Init();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}