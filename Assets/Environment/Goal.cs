using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour
{
    [SerializeField]
    GameObject goalLight;

    [SerializeField]
    string nextScene;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            var g = Instantiate(goalLight, transform.position, Quaternion.identity);
            g.GetComponent<Rigidbody2D>().velocity = -transform.up * 15f;
            StartCoroutine(FinishLevel());
        }
    }

    IEnumerator FinishLevel()
    {
        var timer = 0f;
        while (true)
        {
            timer += Time.deltaTime;
            if (timer > 1f)
            {
                SceneManager.LoadScene(nextScene);
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
