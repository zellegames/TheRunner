using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{
    [SerializeField]
    float power;

    [SerializeField]
    bool reverse;

    [SerializeField]
    GameObject center;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Player.Runner>().Launch(transform.up, power, reverse, GetComponent<Collider2D>());
            //collision.gameObject.transform.position = (center.transform.position);
        }
    }
}
