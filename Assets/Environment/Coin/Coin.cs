using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{

    [SerializeField]
    AudioClip pickup;

    [SerializeField]
    GameObject particles;

    AudioSource aSource;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(particles, transform.position, Quaternion.identity);
            collision.gameObject.GetComponent<AudioSource>().PlayOneShot(pickup, 0.5f);
            collision.gameObject.GetComponent<Player.Runner>().PickupCoin();
            Destroy(gameObject);
        }
    }
}
