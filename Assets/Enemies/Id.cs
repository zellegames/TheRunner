using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Enemies
{

    public class Id : MonoBehaviour
    {
        [SerializeField]
        TextMeshPro text;

        [SerializeField]
        TextMeshPro distanceText;

        [SerializeField]
        LineRenderer lineRenderer;

        GameObject player;
        Coroutine display = null;

        private void Update()
        {
            if (player == null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
                return;
            }

            var distance = Mathf.Abs(Vector2.Distance(player.transform.position, transform.parent.position));
            if (distance < 5f)
            {
                distanceText.text = string.Format("{0:.##}", distance * 10f);
                if (display == null)
                {
                    display = StartCoroutine(Display());
                }
            }
            else
            {
                Clear();
            }

            transform.rotation = Quaternion.Euler(0.0f, 0.0f, transform.parent.rotation.z * -1.0f);
        }

        IEnumerator Display()
        {
            for (var i = 0; i != 10; i++)
            {
                lineRenderer.enabled = !lineRenderer.enabled;
                text.enabled = !text.enabled;
                distanceText.enabled = !distanceText.enabled;
                yield return new WaitForSeconds(0.02f);
            }

            lineRenderer.enabled = true;
            text.enabled = true;
            distanceText.enabled = true;
        }

        void Clear()
        {
            if (display != null)
            {
                StopCoroutine(display);
                display = null;
            }
            lineRenderer.enabled = false;
            text.enabled = false;
            distanceText.enabled = false;
        }
    }
}
