using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Antivirus : MonoBehaviour
{
    [SerializeField]
    AntivirusData antivirusData;

    [SerializeField]
    GameObject particles;

    [SerializeField]
    AudioClip explode;

    [SerializeField]
    bool homing;

    [SerializeField]
    float dirOverride;

    [SerializeField]
    float speedOverride;

    Rigidbody2D rb;
    GameObject target;
    Vector2 targetLead;
    Vector2 direction;
    float targetAngle;
    bool activated;
    bool locked;

    public void Activate(GameObject target)
    {
        if (dirOverride == 0f)
        {
            this.target = target;
            targetLead = target.GetComponent<Collider2D>().ClosestPoint(transform.position);
            direction = (targetLead - (Vector2)transform.position).normalized;
            targetAngle = Vector2.SignedAngle(Vector2.right, direction);
        } else
        {
            targetAngle = dirOverride;
        }
        activated = true;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (!activated || locked) { return; }

        if (dirOverride == 0f)
        {
            var dp = Vector2.Dot(direction, transform.right);
            if (dp < 0.99f)
            {
                rb.MoveRotation(Mathf.LerpAngle(transform.eulerAngles.z, targetAngle, Time.deltaTime * antivirusData.TurnSpeed));
            } else
            {
                locked = true;
                return;
            }
        }
        else
        {
            if (Mathf.Abs(transform.eulerAngles.z - targetAngle) > 0.5f)
            {
                rb.MoveRotation(Mathf.LerpAngle(transform.eulerAngles.z, targetAngle, Time.deltaTime * antivirusData.TurnSpeed));
            }
            else
            {
                locked = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (locked)
        {
            rb.velocity = transform.right * ((speedOverride == 0) ? antivirusData.Speed : speedOverride);
            if (homing)
            {
                var tAngle = Vector2.SignedAngle(Vector2.right, (target.transform.position - transform.position).normalized);
                rb.MoveRotation(Mathf.LerpAngle(rb.rotation, tAngle, Time.deltaTime * 3f));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<AudioSource>().PlayOneShot(explode, 0.5f);
            collision.gameObject.GetComponent<Player.Runner>().TakeDamage(new Player.HitData(antivirusData.Damage, transform.position.x < collision.gameObject.transform.position.x));
            Instantiate(particles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("ground"))
        {
            GameObject.FindGameObjectWithTag("Player")?.GetComponent<AudioSource>().PlayOneShot(explode, 0.5f);
            Instantiate(particles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
