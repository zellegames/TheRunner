using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy Data/Antivirus")]
public class AntivirusData : ScriptableObject
{
    [SerializeField]
    float timeToActivate;

    [SerializeField]
    float speed;

    [SerializeField]
    float turnSpeed;

    [SerializeField]
    float leadDistance;

    [SerializeField]
    float damage;


    public float TimeToActivate { get { return timeToActivate; } }
    public float Speed { get { return speed; } }
    public float TurnSpeed { get { return turnSpeed; } }
    public float LeadDistance { get { return leadDistance; } }
    public float Damage { get { return damage; } }
}
