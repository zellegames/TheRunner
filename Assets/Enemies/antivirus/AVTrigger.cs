using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AVTrigger : MonoBehaviour
{
    [SerializeField]
    Antivirus[] triggers;

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            foreach (var t in triggers)
            {
                if (t != null)
                {
                    t.Activate(collision.gameObject);
                }
            }
        }
    }

}
