using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sounds
{
    public class BG : MonoBehaviour
    {
        static BG instance;

        [SerializeField]
        Player.StateMachine playerStateMachine;

        [SerializeField]
        float minBGVolume;

        [SerializeField]
        float maxBGVolume;

        [SerializeField]
        float volumeSpeed;

        AudioSource aSource;

        public static BG GetInstance()
        {
            return instance;
        }

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            } else
            {
                instance = this;
                aSource = GetComponent<AudioSource>();
                DontDestroyOnLoad(gameObject);
            }
        }

        private void Update()
        {
            //var targetVolume = Mathf.Min(Mathf.Lerp(minBGVolume, maxBGVolume, playerStateMachine.Runner.RB.velocity.magnitude / playerStateMachine.PlayerData.MaxSpeed), maxBGVolume);
            //aSource.volume = Mathf.Lerp(aSource.volume, targetVolume, Time.deltaTime * volumeSpeed);
        }
    }
}
