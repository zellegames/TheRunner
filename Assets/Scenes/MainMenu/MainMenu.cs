using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    Button startButton;

    private void Awake()
    {
        startButton.onClick.AddListener(HandleStartClick);
    }

    void HandleStartClick()
    {
        SceneManager.LoadScene("Debug", LoadSceneMode.Single);
    }
}
