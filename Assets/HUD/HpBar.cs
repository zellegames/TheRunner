using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    public Slider Slider;
    public Slider DelayBarSlider;
    public Player.PlayerData PlayerData;
    public TMPro.TextMeshProUGUI MC;
    public TMPro.TextMeshProUGUI MT;

    float lastHitAmount = 100f;

    private void Start()
    {
        Slider.value = 1f;
        DelayBarSlider.value = 1f;
    }

    private void Update()
    {
        var ts = TimeSpan.FromSeconds(PlayerData.MissionTimer);
        MT.text = "MISSION TIME:   " + ts.ToString(@"hh\:mm\:ss\.fff");

        MC.text = "MEMORY CELLS: " + PlayerData.Coins + " / " + PlayerData.CoinCount;
        Slider.value = PlayerData.HP / 100f;
        if (lastHitAmount != PlayerData.HP)
        {
            StartCoroutine(animateBar(lastHitAmount, PlayerData.HP));
        }
        lastHitAmount = PlayerData.HP;
    }

    IEnumerator animateBar(float oldHp, float newHp)
    {
        var step = 0f;
        var target = newHp / 100f;
        var sliderValue = DelayBarSlider.value;
        while (!Mathf.Approximately(1f, step))
        {
            step += 0.05f;
            DelayBarSlider.value = Mathf.Lerp(sliderValue, target, step);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
