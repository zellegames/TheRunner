using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    [SerializeField]
    Button continueButton;

    [SerializeField]
    Player.StateMachine playerStateMachine;

    [SerializeField]
    Player.Runner playerPrefab;

    [SerializeField]
    GameObject coinCount;

    private void Awake()
    {
        continueButton.onClick.AddListener(ContinueGame);
    }

    private void Update()
    {
        var t = coinCount.GetComponent<TMPro.TextMeshProUGUI>();
        t.text = "CHIPS COLLECTED: " + playerStateMachine.PlayerData.Coins + " / " + playerStateMachine.PlayerData.CoinCount;
        t.text += "\nMISSION TIME: " + System.TimeSpan.FromSeconds(playerStateMachine.PlayerData.MissionTimer).ToString(@"hh\:mm\:ss\.fff");
    }

    void ContinueGame()
    {
        playerStateMachine.Init();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
