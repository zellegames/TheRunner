using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Player;

public class DebugPanel : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI debugText;

    [SerializeField]
    Button saveButton;

    [SerializeField]
    Button loadButton;

    [SerializeField]
    Button knockBackLeft;

    [SerializeField]
    Button knockBackRight;

    [SerializeField]
    Button reverseButton;

    [SerializeField]
    StateMachine stateMachine;

    void Awake()
    {
        saveButton.onClick.AddListener(SavePlayer);
        loadButton.onClick.AddListener(LoadPlayer);
        knockBackLeft.onClick.AddListener(KnockbackLeft);
        knockBackRight.onClick.AddListener(KnockbackRight);
        reverseButton.onClick.AddListener(Reverse);
    }

    private void OnGUI()
    {
        debugText.text = "ST: " + stateMachine.CurrentState.ToString();
        debugText.text += "\nHP: " + stateMachine.PlayerData.HP;
        debugText.text += "\nC: " + stateMachine.PlayerData.Coins;
        if (stateMachine.Runner != null)
        {
            debugText.text += "\nVEL: " + stateMachine?.Runner?.RB?.velocity;
            debugText.text += "\nVMAG:" + stateMachine.Runner.RB.velocity.magnitude;
            debugText.text += "\nSLD: " + stateMachine.Runner.Sliding;
        }
    }

    void SavePlayer()
    {
        
    }

    void LoadPlayer()
    {
       
    }

    void KnockbackLeft()
    {
        stateMachine.Runner.TakeDamage(new HitData(1, false));
    }

    void KnockbackRight()
    {
        stateMachine.Runner.TakeDamage(new HitData(1, true));
    }

    void Reverse()
    {
        stateMachine.SetReverse(!stateMachine.Reverse);
    }
}
